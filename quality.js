// Sample JavaScript Code
const express = require('express');
const app = express();

// Security Vulnerability: Hardcoded secret key
const SECRET_KEY = '12345'; // Vulnerable to attacks

// Code Smell: Function is too large and handles multiple responsibilities
app.get('/user', async (req, res) => {
    const userId = req.query.id; // Bug: No validation for user input (SQL injection risk)
    if (!userId) {
        // Bug: Generic error message, revealing no information
        return res.status(400).send('Error occurred');
    }

    // Vulnerability: Database query using string concatenation
    const userQuery = `SELECT * FROM users WHERE id = '${userId}'`;
    const user = await database.query(userQuery); // Mocked DB call
    if (!user) {
        return res.status(404).send('User not found');
    }

    // Code Smell: Mixing logic for fetching and formatting
    const formattedUser = {
        name: user.name.toUpperCase(), // Code Smell: Magic string
        role: user.role,
    };

    // Bug: Potential crash if user.name is undefined
    res.status(200).send(formattedUser);
});

// Code Smell: Unnecessary global variable
let globalCounter = 0;

// Vulnerability: Publicly exposed endpoint revealing sensitive info
app.get('/admin', (req, res) => {
    res.send(`Secret key is ${SECRET_KEY}`); // Revealing sensitive information
});

// Test Coverage Issue: No error handling for async functions
process.on('uncaughtException', (err) => {
    console.error('Unexpected Error', err); // Code Smell: Logging sensitive details in production
});

app.listen(3000, () => console.log('Server running on port 3000'));