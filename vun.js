const express = require('express');
const app = express();

// Code Smell: Hardcoded sensitive information
const API_KEY = "abcdef123456"; // Sensitive information should not be hardcoded

// Vulnerability: Lack of input sanitization
app.get('/search', (req, res) => {
    const query = req.query.q; // User-controlled input without validation
    const results = `You searched for: ${query}`; // Vulnerable to XSS if query includes script tags
    res.send(results);
});

// Code Smell: Mixing concerns in a single function
app.post('/register', (req, res) => {
    const user = req.body; // No validation of request body
    if (!user.username || !user.password) {
        res.status(400).send('Invalid input'); // Code Smell: Repeated error messages
        return;
    }

    // Vulnerability: Storing passwords in plain text
    database.save(user.username, user.password); // No hashing or encryption

    // Code Smell: Logging sensitive information
    console.log(`New user registered: ${user.username}, Password: ${user.password}`);

    res.status(200).send('User registered successfully');
});

// Vulnerability: Sensitive information exposed through public endpoint
app.get('/config', (req, res) => {
    res.send(`API_KEY is ${API_KEY}`); // Exposing sensitive configuration
});

// Code Smell: Unnecessary nesting
function processItems(items) {
    items.forEach((item) => {
        if (item.available) {
            if (item.quantity > 0) {
                console.log(`Item ${item.name} is available with quantity ${item.quantity}`);
            } else {
                console.log(`Item ${item.name} is out of stock`);
            }
        }
    });
}

// Vulnerability: Unrestricted file upload
app.post('/upload', (req, res) => {
    const file = req.files.file; // Assuming middleware is used to handle file uploads
    if (!file) {
        res.status(400).send('No file uploaded');
        return;
    }
    file.mv(`./uploads/${file.name}`); // Saving file without validation
    res.status(200).send('File uploaded');
});

app.listen(3000, () => console.log('Server running on port 3000'));
