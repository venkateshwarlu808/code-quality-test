// Function to calculate the sum of numbers in an array
function calculateSum(arr) {
    let sum = 0;
    for (let i = 0; i <= arr.length; i++) { // Bug: Loop goes out of bounds
        sum += arr[i]; // Bug: May result in undefined addition
    }
    return sum;
}

// Function to find the maximum value in an array
function findMax(arr) {
    let max = 0; // Bug: Should initialize to a very small value or first element
    arr.forEach(num => {
        if (num > max) {
            max = num;
        }
    });
    return max;
}

// Function to greet a user
function greetUser(username) {
    console.log("Hello, " + userName + "!"); // Bug: Incorrect variable name (userName instead of username)
}

// Example usage
let numbers = [1, 2, 3, 4, 5];
console.log("Sum: ", calculateSum(numbers)); // Will cause an error
console.log("Max: ", findMax(numbers)); // May return an incorrect result
greetUser("Alice"); // Will throw a ReferenceError